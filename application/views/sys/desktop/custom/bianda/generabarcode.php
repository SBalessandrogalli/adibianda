<?php
$serverurl=server_url();
?>

<script type="text/javascript">
     // Or with jQuery

  $(document).ready(function(){
    $('input.autocomplete').autocomplete({
      data: {
        <?php
        foreach ($pazienti as $key => $paziente) {
        ?>
            "<?=$paziente['F1002'].' '.$paziente['F1003'].'-'.$paziente['F1023']?>": null,
        <?php
        }
        ?>

      }
    });
    
    $('.modal').modal();
    $('.datepicker').datepicker({
    format: 'dd/mm/yyyy'
    });
  });
  
  
    function creacopertina()
  {
        $('#modal1').modal('open');
        var serialized=$('#generabarcode').find("select,textarea,input").serializeArray();
        $.ajax( {
            type: "POST",
            url: '<?=site_url('sys_viewcontroller/custom_bianda_generabarcode')?>',
            data: serialized,
            success: function( response ) {
                console.info(response);
                alert('ok');
            },
            error:function(){
            }
        } );
  }
  
   function creacopertina_pdf()
  {
        $('#modal1').modal('open');
        var serialized=$('#generabarcode').find("select,textarea,input").serializeArray();
        $.ajax( {
            type: "POST",
            url: '<?=site_url('sys_viewcontroller/custom_bianda_generabarcode_pdf')?>',
            data: serialized,
            success: function( response ) {
                console.info('<?=$serverurl?>'+response);
                window.open('<?=$serverurl?>/'+response, '_blank')
            },
            error:function(){
            }
        } );
  }
</script>

    <nav>
        <div class="nav-wrapper" style="background-color: #2bbbad !important;padding-left: 20px">
            <a href="#" class="brand-logo">Genera barcode</a>
        </div>
    </nav>
    <div id="generabarcode"  class="row">
        <div class="col s8">
          <div class="row">
            <div class="input-field col s12">
              <i class="material-icons prefix">account_circle</i>
              <input type="text" id="autocomplete-input" name="paziente" class="autocomplete">
              <label for="autocomplete-input">Paziente</label>
            </div>
          </div>
        
           
        </div>

        
    </div>
<div class="row">
    <div class="col m6" >
            
                <div class="btn waves-effect waves-light" onclick="creacopertina_pdf()" >Crea Copertina Barcode PDF
                    <i class="material-icons right">send</i>
                </div>
            
    </div>
    
</div>
<div class="row">
    <div class="col m6">
        <div onclick="bianda_load_menu(this)" class="waves-effect waves-light btn" style="width: 100%">indietro</div>
    </div>
</div>    
    




