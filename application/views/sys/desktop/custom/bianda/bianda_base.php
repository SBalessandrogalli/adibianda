 <?php
$serverurl=server_url();
?>       
  <!DOCTYPE html>
  <html>
    <head>
      <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body style="background-color: #white; ">

      <!--JavaScript at end of body for optimized loading-->
      <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="<?=base_url("/assets/js/custom/winteler/quagga.js")?>"></script>
    <script src="<?=base_url("/assets/js/custom/winteler/file_input.js")?>"></script>
    
    <style>
        .collapsible-body{
            padding-top: 0px !important;
        }
        

        .btn{
            
            width: 100%;
        }
        
        .modal{
            max-height: none;
            width: 90%;
            height: 90%;
        }
        
        .card{
            padding: 10px !important;
            text-align: center;
        }
        
 
        
    </style>
    <script type="text/javascript">

    function bianda_load_menu(el)
    {
         $.ajax({
             url: '<?= controller_url()."bianda_load_menu" ?>',
             method: 'url',
             success:function(response){
               $('.card-content').html(response);
             },
             error:function(){
                 alert('errore');
             }
         });
    }
       
    
    function bianda_view_generabarcode(el)
    {
        var url='<?= controller_url()?>/custom_bianda_view_generabarcode';
         $.ajax({
             url: url,
             method: 'url',
             success:function(response){
               $('.card-content').html(response);
             },
             error:function(){
                 alert('errore');
             }
         });
    }
    
    function bianda_view_ricercadocumenti(el)
    {
        var url='<?= controller_url()?>/custom_bianda_view_ricercadocumenti';
         $.ajax({
             url: url,
             method: 'url',
             success:function(response){
               $('.card-content').html(response);
             },
             error:function(){
                 alert('errore');
             }
         });
    }
    
    function bianda_logout(el)
    {
        $.ajax({
                url: '<?= controller_url()."bianda_logout" ?>',
                method: 'url',
                success:function(response){
                  $('.card-content').html(response);
                },
                error:function(){
                    alert('errore');
                }
            });
    }
    
    
    function bianda_view_documenti(el)
    {
        $.ajax({
                url: '<?= controller_url()."bianda_view_documenti" ?>',
                method: 'url',
                success:function(response){
                  $('#bianda_documenti_container').html(response);
                },
                error:function(){
                    alert('errore');
                }
            });
    }
      
    
    
        
    </script>
        <div class="container" style="background-color: #fdfdfd">
            <div class="row" style="background: transparent">
                <div class="col s12 m12  card">
                    <div class="card-image" style="font-size: 25px">
                        STUDIO MEDICO BIANDA CAMILLO
                    </div>
                    <div class="card-content" style="//background-image:url('<?=base_url("/assets/images/custom/Winteler/mercedes.jpg")?>');background-repeat: no-repeat;background-size: cover; ">
                            
                        <?=$content?>
                        
                    </div>
                </div>
            </div>
        </div>
    </body>
  </html>