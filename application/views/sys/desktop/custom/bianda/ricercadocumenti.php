<?php
?>

<script type="text/javascript">
     // Or with jQuery

  $(document).ready(function(){
    $('input.autocomplete').autocomplete({
      data: {
        <?php
        foreach ($pazienti as $key => $paziente) {
        ?>
            "<?=$paziente['F1002'].' '.$paziente['F1003'].'-'.date('d/m/Y', strtotime($paziente['F1006']))?>": null,
        <?php
        }
        ?>

      }
    });
    
    $('select').formSelect();
    
  });
  
</script>

<nav>
    <div class="nav-wrapper" style="background-color: #2bbbad !important;padding-left: 20px">
        <a href="#" class="brand-logo">Ricerca documenti</a>
    </div>
</nav>
<div class="row">
    <div class="col s12">
        <div class="row">
          <div class="input-field col s6">
            <i class="material-icons prefix">account_circle</i>
            <input type="text" id="autocomplete-input" name="paziente" class="autocomplete">
            <label for="autocomplete-input">Paziente</label>
          </div>
            <div class="input-field col s3">
                    <select>
                      <option value="" disabled selected>Choose your option</option>
                      <option value="1">DOC</option>
                      <option value="2">ECG</option>
                    </select>
                    <label>Tipo</label>
            </div>
            <div class="input-field col s12">
                <input type="text" id="fulltext">
                <label for="fulltext">Fulltext</label>
            </div>
            
        </div>
          


    </div>
</div>
<div class="row">
    <div class="col m6">
        <div onclick="bianda_view_documenti(this)" class="waves-effect waves-light btn" style="width: 100%">Ricerca</div>
    </div>
</div>  
<div id="bianda_documenti_container">
    
</div>

  

    
</div>
