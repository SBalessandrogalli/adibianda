
<script type="text/javascript">
     // Or with jQuery

  $(document).ready(function(){
    $('.modal').modal();
    
    $(".zoom").hover(function(){
        $('#modal_preview').modal('open');
        $('#modal_preview').find('img').attr('src',($(this).closest('.thumbnail').find('img').attr('src')));;
    });
    
    $('#modal_preview').mouseout(function(){
        $('#modal_preview').modal('close');
    });
  });
  
  function apri_documento(el,fidd)
  {
      $.ajax({
             url: '<?= controller_url()."bianda_scarica_documento"?>/'+fidd,
             method: 'url',
             success:function(response){
               $('#modal_pdf').modal('open');
               $('#modal_pdf').find('#pdfviewer').attr('src',response);
             },
             error:function(){
                 alert('errore');
             }
         });
  }
  
 
  
</script>
<?php
$counter=0;
foreach ($documenti as $key => $documento) {
    $counter++;
    $path=$documento['path'];
    $path_preview= str_replace(".PDF", "_preview.png", $path);
    $path_preview= str_replace(".pdf", "_preview.png", $path_preview);

?>
<div id="modal_preview" class="modal" style="width: 30%">
    <div class="modal-content">
        <img src="" style="width: 100%"></img>
    </div>
</div>

<div id="modal_pdf" class="modal" style="width: 50%">
    <div class="modal-content" style="height: 100%">
        <iframe id="pdfviewer" src="" style="width: 100%;height: 100%" ></iframe>
    </div>
</div>

<div class="col m3">
    
    <div class="card thumbnail" onclick="apri_documento(this,'<?=$documento['fidd']?>')" style="height: 300px"  >
        <div>
        <span class="material-icons zoom" style="float: left">search</span>
        <?=$documento['cognomenome']?> <br/>
        <?=$documento['tipo']?>
        </div>
        <img src="../JDocServer/Preview/<?=$path_preview?>" width="100%"></img>
    </div>
</div>
     
          

<?php

}
?>