<!DOCTYPE html>
<html>
<head>
<link href='https://fonts.googleapis.com/css?family=Libre Barcode 39' rel='stylesheet'>
<style>
.barcode {
    font-family: 'Libre Barcode 39';
    font-size: 74px;
    text-align: center;
}
</style>
</head>
<body>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<div style="text-align: center;font-size: 20px">Copertina Paziente</div>
<br><br/>
<div style="text-align: center;font-size: 25px"><?=strtoupper($cognome)?></div>
<div style="text-align: center;font-size: 25px"><?= strtoupper($nome)?></div>
<div style="text-align: center"><?=date('d/m/Y', strtotime($datanascita))?></div>
<br/><br/>
    <div class="barcode">*PAZIENTE-<?=$codice_paziente?>*</div>

</body>
</html>